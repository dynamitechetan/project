
var express = require("express"), // Include express.
    twilio = require("twilio"); // Include twilio.

var app = express(); 

app.use(express.static(__dirname + '/app'));

app.get("/twilio/token", function (req, res) {
  var capability = new twilio.Capability( 
    'AC2e778bf79df8fda1e6899658b6ff54d5',
    '6cf2f95fa521c952f57ef3c5d1b4f284'
  );

  capability.allowClientOutgoing('AP48cc61f731070160f67fa3ce0c6e2f70');

  res.send(capability.generate());
});

app.listen(3000, function () {
  console.dir("Express server started on port 3000.");
});